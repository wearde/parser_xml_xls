# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Parsing data from xsl or xml files

### How do I get set up? ###

* Get methods info
```
./loadxls import/index
```

* Import from xls
```
./loadxls import/xls pahtToFileOnServer
```

* Import from xml
```
./loadxls import/xml pahtToFileOnServer
```
