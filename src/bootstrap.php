<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

$configOpenCart = dirname(dirname(dirname(dirname(__DIR__)))) . '/config.php';
if (file_exists($configOpenCart)) {
  require_once($configOpenCart);
}