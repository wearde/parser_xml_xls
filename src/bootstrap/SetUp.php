<?php

namespace amass\parsedata\bootstrap;

use amass\parsedata\core\dispatchers\EventDispatcher;
use amass\parsedata\core\dispatchers\AsyncEventDispatcher;
use amass\parsedata\core\dispatchers\DeferredEventDispatcher;
use amass\parsedata\core\dispatchers\SimpleEventDispatcher;
use amass\parsedata\core\entities\events\LoadOwnerImage;
use amass\parsedata\core\jobs\AsyncEventJobHandler;
use amass\parsedata\core\listeners\shop\product\LoadOwnerImageListener;
use yii\base\BootstrapInterface;
use yii\console\ErrorHandler;
use yii\di\Container;
use yii\di\Instance;
use yii\helpers\BaseFileHelper;
use yii\queue\Queue;

class SetUp implements BootstrapInterface
{
  public function bootstrap($app)
  {
    $container = \Yii::$container;

    $container->setSingleton(ErrorHandler::class, function () use ($app) {
      return $app->errorHandler;
    });
    $container->setSingleton(Queue::class, function () use ($app) {
      return $app->get('queue');
    });

    $container->setSingleton(EventDispatcher::class, DeferredEventDispatcher::class);

    $container->setSingleton(DeferredEventDispatcher::class, function (Container $container) {
      /**@var Queue $queue*/
      return new DeferredEventDispatcher(new AsyncEventDispatcher($container->get(Queue::class)));
    });

    $container->setSingleton(SimpleEventDispatcher::class, function (Container $container) {
      return new SimpleEventDispatcher($container, [
        LoadOwnerImage::class => [LoadOwnerImageListener::class],
      ]);
    });

    $container->setSingleton(AsyncEventJobHandler::class, [], [
      Instance::of(SimpleEventDispatcher::class)
    ]);


//        $container->setSingleton(Que::class, function () use ($app) {
//            return $app->get('queue');
//        });

    /*
    $container->setSingleton(Filesystem::class, function () use ($app) {
        return new Filesystem(new Ftp($app->params['ftp']));
    });

    $container->set(ImageUploadBehavior::class, FlySystemImageUploadBehavior::class);
    */
  }
}