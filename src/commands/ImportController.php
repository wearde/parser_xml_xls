<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\commands;

use amass\parsedata\core\entities\shop\OwnerAttribute;
use amass\parsedata\core\entities\shop\OwnerImage;
use amass\parsedata\core\entities\shop\UrlAlias;
use amass\parsedata\core\forms\manage\shop\category\CategoryCreateForm;
use amass\parsedata\core\forms\manage\shop\product\ProductCreateForm;
use amass\parsedata\core\helpers\DataHelper;
use amass\parsedata\core\services\manager\shop\CategoryManagerService;
use amass\parsedata\core\services\manager\shop\ProductManagerService;
use amass\parsedata\core\services\parser\xls\Config;
use amass\parsedata\core\services\parser\xls\ReadFilter;
use amass\parsedata\core\services\parser\xml\SimpleXmlStreamer;
use amass\parsedata\core\services\TransactionService;
use DOMDocument;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

class ImportController extends Controller
{
  public $file;
  /**
   * @var ProductManagerService
   */
  private $productManagerService;
  /**
   * @var CategoryManagerService
   */
  private $categoryManagerService;
  /**
   * @var TransactionService
   */
  private $transactionService;

  /**
   * ImportController constructor.
   * @param string $id
   * @param Module $module
   * @param ProductManagerService $productManagerService
   * @param CategoryManagerService $categoryManagerService
   * @param TransactionService $transactionService
   * @param array $config
   */
  public function __construct(
    $id, Module $module,
    ProductManagerService $productManagerService,
    CategoryManagerService $categoryManagerService,
    TransactionService $transactionService,
    array $config = []
  )
  {
    parent::__construct($id, $module, $config);
    $this->productManagerService = $productManagerService;
    $this->categoryManagerService = $categoryManagerService;
    $this->transactionService = $transactionService;
  }

  /**
   * @inheritdoc
   */
  public function options($actionID)
  {
    return array_merge(
      parent::options($actionID),
      ['file']
    );
  }

  public function actionIndex()
  {
    $this->stdout("Implement just two methods XML 'import/xml' & XLS 'import/xls'. If you need clear data just run import/clear.\n\n", Console::FG_GREEN);
  }

  public function actionXls()
  {
    if (!$this->file) {
      $this->file = dirname(dirname(__DIR__)) . '/temp.xlsx';
    }
    if (!file_exists($this->file)) {
      $this->stdout("File '$this->file' not exist.\n\n", Console::FG_RED);
      return;
    }
    $extensionArray = explode('.', $this->file);
    $extension = end($extensionArray);

    if (!$extension == 'xlsx' || !$extension == 'xls') {
      $this->stdout("File '$this->file' is not Excel .\n\n", Console::FG_RED);
      return;
    }

    /** @var \PHPExcel_Reader_Excel2007 $objReader */
    $objReader = \PHPExcel_IOFactory::createReaderForFile($this->file);

    $chunkSize = 30;
    $chunkFilter = new ReadFilter();
    $objReader->setReadFilter($chunkFilter);
    $objReader->setReadDataOnly(true);
    $totalRows = 50000;

    for ($startRow = 2; $startRow <= $totalRows; $startRow += $chunkSize) {

      $chunkFilter->setRows($startRow, $chunkSize);
      $objPHPExcel = $objReader->load($this->file);

      $sheetData = $objPHPExcel->getActiveSheet()->toArray();

      if (!empty($sheetData) && $startRow < $totalRows) {
        foreach ($sheetData as $datum) {
          $datum = array_filter($datum);
          if (empty($datum)) {
            continue;
          }
          $categories = explode('->', ArrayHelper::getValue($datum, Config::FULL_CATEGORIES));
          $parent = '';
          foreach ($categories as $category) {
            $categoryForm = new CategoryCreateForm();

            $categoryForm->owner_parent_name = $parent;
            $categoryForm->categoryDescription->name = $category;
            $categoryForm->categoryDescription->description = $category;
            $categoryForm->categoryDescription->meta->title = $category;
            $categoryForm->categoryDescription->meta->description = $category;
            $categoryForm->categoryDescription->meta->keywords = $category;

            $data = DataHelper::field([
              $categoryForm,
              $categoryForm->categoryDescription,
              $categoryForm->categoryDescription->meta
            ]);
            if ($categoryForm->load($data)) {
              $this->categoryManagerService->create($categoryForm);
            }
            $parent = $category;
          }

          $productForm = new ProductCreateForm();

          $productForm->sku = (string)ArrayHelper::getValue($datum, Config::SKU);
          $productForm->model = (string)ArrayHelper::getValue($datum, Config::SKU);
          $productForm->weight = 0;

          $productForm->category_name = ArrayHelper::getValue($datum, Config::SUB_CATEGORY);
          $productForm->quantity = ArrayHelper::getValue($datum, Config::QUANTITY);
          $productForm->status = 1;

          $productForm->productDescription->name = ArrayHelper::getValue($datum, Config::NAME);
          $productForm->productDescription->description = ArrayHelper::getValue($datum, Config::NAME);
          $productForm->productDescription->meta->title = ArrayHelper::getValue($datum, Config::NAME);
          $productForm->productDescription->meta->description = ArrayHelper::getValue($datum, Config::NAME);
          $productForm->productDescription->meta->keywords = ArrayHelper::getValue($datum, Config::SKU) . ', ' . ArrayHelper::getValue($datum, Config::NAME);
          $productForm->price->new = ArrayHelper::getValue($datum, Config::PRICE);
          $productForm->manufacturer->name = ArrayHelper::getValue($datum, Config::MANUFACTURER);

          $images = explode(' ', ArrayHelper::getValue($datum, Config::IMAGES));
          $productForm->main_image = 'data/' . reset($images);

          $productForm->photos->images = array_map(function ($image) {
            return new OwnerImage($image, 'data/' . $image);
          }, $images, []);

          $attributesHtml = ArrayHelper::getValue($datum, Config::ATTRIBUTES);

          $DOM = new DOMDocument;
          $DOM->loadHTML('<?xml encoding="UTF-8">' . $attributesHtml);

          $attributes = $DOM->getElementsByTagName('tr');
          foreach ($attributes as $node) {
            $name = $node->childNodes->item(0)->nodeValue;
            $value = $node->childNodes->item(1)->nodeValue;
            $productForm->values->data[] = new OwnerAttribute($name, $value);
          }

          $data = DataHelper::field([
            $productForm,
            $productForm->productDescription,
            $productForm->productDescription->meta,
            $productForm->price,
            $productForm->manufacturer,
            $productForm->photos,
            $productForm->ownerImages,
            $productForm->values
          ]);

          if ($productForm->load($data) && $productForm->validate()) {
            $this->productManagerService->create($productForm);
          }
        }
      }
      $objPHPExcel->disconnectWorksheets();
      unset($objPHPExcel, $sheetData);
    }
  }

  public function actionXml()
  {
    if (!$this->file) {
      $this->file = dirname(dirname(__DIR__)) . '/xml.xml';
    }
    if (!file_exists($this->file)) {
      $this->stdout("File '$this->file' not exist.\n\n", Console::FG_RED);
      return;
    }
    $extensionArray = explode('.', $this->file);
    $extension = end($extensionArray);

    if (!$extension == 'xml') {
      $this->stdout("File '$this->file' is not XML .\n\n", Console::FG_RED);
      return;
    }

    $streamer = new SimpleXmlStreamer($this->file);
    // categories
    try {
      $streamer->setCustomRootNode('categories');
      $streamer->setCloseWhenFinished(false);

      if ($streamer->setCallBack(function ($xml) {
        /** @var \SimpleXMLElement $xml */
        $categoryForm = new CategoryCreateForm();
        $attributes = (array)$xml->attributes();
        $attributes = current($attributes);
        $categoryForm->owner_id = ArrayHelper::getValue($attributes, 'id');
        $categoryForm->owner_parent_id = ArrayHelper::getValue($attributes, 'parent_id', 0);

        $categoryForm->categoryDescription->name = (string)$xml;
        $categoryForm->categoryDescription->description = (string)$xml;
        $categoryForm->categoryDescription->meta->title = (string)$xml;
        $categoryForm->categoryDescription->meta->description = (string)$xml;
        $categoryForm->categoryDescription->meta->keywords = (string)$xml;

        $data = DataHelper::field([
          $categoryForm,
          $categoryForm->categoryDescription,
          $categoryForm->categoryDescription->meta
        ]);

        if ($categoryForm->load($data)) {
          $this->categoryManagerService->create($categoryForm);
        }

      })->parse()) {
        echo "Finished successfully";
      } else {
        echo "Couldn't find root node";
      }
    } catch (\Exception $exception) {
      throw new \DomainException($exception->getMessage(), null, $exception);
    }

    // products
    $streamer->setCustomRootNode('products');
    $streamer->setCloseWhenFinished(true);
    if ($streamer->setCallBack(function ($xml) {
      /** @var \SimpleXMLElement $xml */
      $productForm = new ProductCreateForm();
      $xml = (array)$xml;
      $productForm->sku = ArrayHelper::getValue($xml, 'sku');
      $productForm->model = ArrayHelper::getValue($xml, 'sku');
      $productForm->weight = ArrayHelper::getValue($xml, 'weight');
      $productForm->product_url = ArrayHelper::getValue($xml, 'product_url');
      $productForm->category_id = ArrayHelper::getValue($xml, 'category_id');

      $productForm->productDescription->name = ArrayHelper::getValue($xml, 'name');
      $productForm->productDescription->description = ArrayHelper::getValue($xml, 'description');
      $productForm->productDescription->meta->title = ArrayHelper::getValue($xml, 'name');
      $productForm->productDescription->meta->description = ArrayHelper::getValue($xml, 'description');
      $productForm->productDescription->meta->keywords = ArrayHelper::getValue($xml, 'sku') . ', ' . ArrayHelper::getValue($xml, 'name');
      $productForm->price->new = ArrayHelper::getValue($xml, 'price');
      $productForm->manufacturer->name = ArrayHelper::getValue($xml, 'manufacturer');
      $productForm->manufacturer->name = ArrayHelper::getValue($xml, 'manufacturer');

      $images = array_values((array)ArrayHelper::getValue($xml, 'images'));
      $images = reset($images);
      if ($images instanceof \SimpleXMLElement) {
        $images = [$images];
      }

      $productForm->ownerImages->images = array_map(function ($image) {
        $name = ArrayHelper::getValue((array)$image, 'name');
        $url = ArrayHelper::getValue((array)$image, 'url');
        return new OwnerImage($name, $url);
      }, $images, []);

      $attributes = array_values((array)ArrayHelper::getValue($xml, 'features'));
      $attributes = reset($attributes);
      if ($attributes instanceof \SimpleXMLElement) {
        $attributes = [$attributes];
      }
      $productForm->values->data = array_map(function ($attribute) {
        $name = ArrayHelper::getValue((array)$attribute, 'name');
        $value = trim(str_replace($name . ':', '', ArrayHelper::getValue((array)$attribute, 'value')));
        return new OwnerAttribute($name, $value);
      }, $attributes, []);

      $data = DataHelper::field([
        $productForm,
        $productForm->productDescription,
        $productForm->productDescription->meta,
        $productForm->price,
        $productForm->manufacturer,
        $productForm->ownerImages,
        $productForm->photos,
        $productForm->values
      ]);

      if ($productForm->load($data) && $productForm->validate()) {
        $this->productManagerService->create($productForm);
      }

    })->parse()) {
      echo "Finished successfully";
    } else {
      echo "Couldn't find root node";
    }
  }

  public function actionClear()
  {
    $this->transactionService->wrap(function () {
      $this->categoryManagerService->clearData();
      $this->productManagerService->clearData();
      UrlAlias::deleteAll();
    });
  }
}