<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\commands;

use yii\console\Controller;
use yii\helpers\Inflector;

/**
 * Generate gii from opencart Bd.
 * Class OpencartgController
 * @package amass\parsedata\commands
 */
class OpencartgController extends Controller
{
  /**
   * @var string controller default action ID.
   */
  public $defaultAction = 'model';

  /**
   * @var array of tables in DB.
   * * For example, `--tables="product,category"`
   */
  public $tables = [];

  public $ns = 'app\model';

  public $useTablePrefix = 1;
  /**
   * Generate all models added to property $pathToConfig
   */
  public function actionModel()
  {
    foreach ($this->tables as $table) {
      \Yii::$app->runAction('gii/model', [
        'tableName' => $this->useTablePrefix
          ? \Yii::$app->db->tablePrefix . $table
          : $table,
        'useTablePrefix' => $this->useTablePrefix,
        'modelClass' => Inflector::camelize($table),
        'interactive' => 0,
        'ns' => $this->ns
      ]);
    }

  }

  /**
   * @inheritdoc
   */
  public function options($actionID)
  {
    return array_merge(
      parent::options($actionID),
      ['tables', 'ns', 'useTablePrefix']
    );
  }
}