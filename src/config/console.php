<?php
if (!defined('DIR_IMAGE')) {
  define('DIR_IMAGE', dirname(dirname(__DIR__)). '/image/');
}

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$bootstrap = require(__DIR__ . '/bootstrap.php');
$config = [
  'id' => 'basic-console',
  'basePath' => dirname(__DIR__),
  'controllerNamespace' => 'amass\parsedata\commands',
  'bootstrap' => $bootstrap,
  'modules' => [
    'gii' =>  [
      'class' => 'yii\gii\Module'
    ]
  ],
  'components' => [
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'queue' => [
      'class' => \yii\queue\file\Queue::class,
      'path' => '@runtime/queue',
      'as log' => 'yii\queue\LogBehavior',
    ],
    'log' => [
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'db' => $db,
  ],
  'params' => $params,
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
  ];
}

return $config;