<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */


if (!defined('DB_DRIVER')) {
  define('DB_DRIVER', 'mysqli');
}
if (!defined('DB_HOSTNAME')) {
  define('DB_HOSTNAME', '127.0.0.1');
}
if (!defined('DB_USERNAME')) {
  define('DB_USERNAME', 'root');
}
if (!defined('DB_PASSWORD')) {
  define('DB_PASSWORD', '');
}

if (!defined('DB_PORT')) {
  define('DB_PORT', '3306');
}

if (!defined('DB_DATABASE')) {
  define('DB_DATABASE', 'shop');
}

if (!defined('DB_PREFIX')) {
  define('DB_PREFIX', 'oc_');
}

return [
  'class' => 'yii\db\Connection',
  'dsn' => 'mysql:host='. DB_HOSTNAME .';port=' . DB_PORT . ';dbname=' . DB_DATABASE,
  'username' => DB_USERNAME,
  'password' => DB_PASSWORD,
  'charset' => 'utf8mb4',
  'tablePrefix' => DB_PREFIX,
  // Schema cache options (for production environment)
  //'enableSchemaCache' => true,
  //'schemaCacheDuration' => 60,
  //'schemaCache' => 'cache',
];