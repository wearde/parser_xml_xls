<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\dispatchers;

use amass\parsedata\core\jobs\AsyncEventJob;
use yii\queue\Queue;

/**
 * Class AsyncEventDispatcher
 * @package amass\parsedata\core\dispatchers
 */
class AsyncEventDispatcher implements EventDispatcher
{
  /**
   * @var Queue
   */
  private $queue;

  /**
   * AsyncEventDispatcher constructor.
   * @param Queue $queue
   */
  public function __construct(Queue $queue)
  {
    $this->queue = $queue;
  }

  /**
   * @param array $events
   */
  public function dispatchAll(array $events)
  {
    foreach ($events as $event) {
      $this->dispatch($event);
    }
  }

  /**
   * @param $event
   */
  public function dispatch($event)
  {
    $this->queue->push(new AsyncEventJob($event));
  }
}