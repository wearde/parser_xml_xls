<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\dispatchers;

/**
 * Class DeferredDispatcher
 * @package amass\parsedata\core\dispatchers
 */
class DeferredEventDispatcher implements EventDispatcher
{
  /**
   * @var bool
   */
  private $defer = false;
  /**
   * @var array
   */
  private $queue = [];
  /**
   * @var EventDispatcher
   */
  private $next;

  /**
   * DeferredDispatcher constructor.
   * @param EventDispatcher $next
   */
  public function __construct(EventDispatcher $next)
  {
    $this->next = $next;
  }

  /**
   * @param array $events
   */
  public function dispatchAll(array $events)
  {
    foreach ($events as $event) {
      $this->dispatch($event);
    }
  }

  /**
   * @param $event
   */
  public function dispatch($event)
  {
    if ($this->defer) {
      $this->queue[] = $event;
    } else {
      $this->next->dispatch($event);
    }
  }

  /**
   *
   */
  public function defer()
  {
    $this->defer = true;
  }

  /**
   *
   */
  public function clean()
  {
    $this->queue = [];
    $this->defer = false;
  }

  /**
   *
   */
  public function release()
  {
    foreach ($this->queue as $i => $event) {
      $this->next->dispatch($event);
      unset($this->queue[$i]);
    }
    $this->defer = false;
  }
}