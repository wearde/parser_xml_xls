<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\dispatchers;

interface EventDispatcher
{
  /**
   * @param array $events
   */
  public function dispatchAll(array $events);

  /**
   * @param $event
   */
  public function dispatch($event);
}