<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\dispatchers;

use yii\di\Container;

/**
 * Class SimpleEventDispatcher
 * @package amass\parsedata\core\dispatchers
 */
class SimpleEventDispatcher implements EventDispatcher
{
  /**
   * @var Container
   */
  private $container;
  /**
   * @var array
   */
  private $listeners;

  /**
   * SimpleEventDispatcher constructor.
   * @param Container $container
   * @param array $listeners
   */
  public function __construct(Container $container, array $listeners)
  {
    $this->container = $container;
    $this->listeners = $listeners;
  }

  /**
   * @param array $events
   */
  public function dispatchAll(array $events)
  {
    foreach ($events as $event) {
      $this->dispatch($event);
    }
  }

  /**
   * @param $event
   */
  public function dispatch($event)
  {
    $eventName = get_class($event);
    if (array_key_exists($eventName, $this->listeners)) {
      foreach ($this->listeners[$eventName] as $listenerClass) {
        $listener = $this->resolveListener($listenerClass);
        $listener($event);
      }
    }
  }

  /**
   * @param $listenerClass
   * @return array
   */
  private function resolveListener($listenerClass)
  {
    return [$this->container->get($listenerClass), 'handle'];
  }
}