<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities;

/**
 * Class Meta
 * @package amass\parsedata\core\entities
 *
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 */
class Meta
{
  public $meta_title;

  public $meta_description;

  public $meta_keyword;

  /**
   * Meta constructor.
   * @param $title
   * @param $description
   * @param $keywords
   */
  public function __construct($title, $description, $keywords)
  {
    $this->meta_title = $title;
    $this->meta_description = $description;
    $this->meta_keyword = $keywords;
  }
}