<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities\behaviors;

use amass\parsedata\core\entities\Meta;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class MetaBehavior
 * @package amass\parsedata\core\entities\behaviors
 */
class MetaBehavior extends Behavior
{
  /**
   * @var string
   */
  public $attribute = 'meta';

  /**
   * @return array
   */
  public function events()
  {
    return [
      ActiveRecord::EVENT_AFTER_FIND => 'onAfterFind',
      ActiveRecord::EVENT_BEFORE_INSERT => 'onBeforeSave',
      ActiveRecord::EVENT_BEFORE_UPDATE => 'onBeforeSave',
      ActiveRecord::EVENT_BEFORE_VALIDATE => 'onBeforeValidate',
    ];
  }

  /**
   * @param Event $event
   */
  public function onAfterFind(Event $event)
  {
    /**
     * @var ActiveRecord $model
     */
    $model = $event->sender;

    $model->{$this->attribute} = new Meta(
      ArrayHelper::getValue($model, 'meta_title'),
      ArrayHelper::getValue($model, 'meta_description'),
      ArrayHelper::getValue($model, 'meta_keyword')
    );
  }

  /**
   * @param Event $event
   */
  public function onBeforeSave(Event $event)
  {
    /**
     * @var ActiveRecord $model
     */
    $model = $event->sender;
    $model->loadDefaultValues();
    $model->setAttributes((array)$model->{$this->attribute});
  }

  /**
   * @param Event $event
   */
  public function onBeforeValidate(Event $event)
  {
    /**
     * @var ActiveRecord $model
     */
    $model = $event->sender;
    $model->setAttributes($model->{$this->attribute});
  }
}