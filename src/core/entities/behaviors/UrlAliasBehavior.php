<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities\behaviors;

use amass\parsedata\core\entities\shop\UrlAlias;
use yii\base\Behavior;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;

/**
 * Class UrlAliasBehavior
 * @package amass\parsedata\core\entities\behaviors
 */
class UrlAliasBehavior extends Behavior
{
  /**
   * @var string
   */
  public $attributeId = null;

  /**
   * @var string
   */
  public $attributeField = 'name';

  /**
   * @throws InvalidConfigException
   */
  public function init()
  {
    parent::init();
    if (null === $this->attributeId) {
      throw new InvalidConfigException('Set primary key attribute to config');
    }
  }

  /**
   * @return array
   */
  public function events()
  {
    return [
      ActiveRecord::EVENT_AFTER_INSERT => 'onAfterSave',
    ];
  }

  /**
   * @param Event $event
   */
  public function onAfterSave(Event $event)
  {
    /** @var ActiveRecord $model */
    $model = $event->sender;
    if(!UrlAlias::find()
      ->where(['query' => $this->attributeId . '=' . $model->{$this->attributeId}])
      ->orWhere(['keyword' => Inflector::slug($model->{$this->attributeField})])
      ->one()) {
      $alias = UrlAlias::create(
        $this->attributeId . '=' . $model->{$this->attributeId},
        Inflector::slug($model->{$this->attributeField})
      );
      $alias->save();
    }
  }
}