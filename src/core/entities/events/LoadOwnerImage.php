<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities\events;

use amass\parsedata\core\entities\shop\OwnerImage;
use amass\parsedata\core\entities\shop\product\Product;

/**
 * Class LoadOwnerImage
 * @package amass\parsedata\core\entities\events
 */
class LoadOwnerImage
{
  /**
   * @var Product
   */
  public $product;
  /**
   * @var OwnerImage
   */
  public $images = [];

  /**
   * LoadOwnerImage constructor.
   * @param Product $product
   * @param OwnerImage[] $images
   */
  public function __construct(Product $product, $images)
  {
    $this->product = $product;
    $this->images = $images;
  }
}