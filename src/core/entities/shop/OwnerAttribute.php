<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities\shop;

class OwnerAttribute
{
  public $name;
  public $value;

  /**
   * OwnerImage constructor.
   * @param $name
   * @param $value
   */
  public function __construct($name, $value)
  {
    $this->name = $name;
    $this->value = $value;
  }
}