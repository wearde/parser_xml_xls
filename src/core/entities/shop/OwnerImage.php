<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities\shop;

class OwnerImage
{
  public $name;
  public $url;

  /**
   * OwnerImage constructor.
   * @param $name
   * @param $url
   */
  public function __construct($name, $url)
  {
    $this->name = $name;
    $this->url = $url;
  }
}