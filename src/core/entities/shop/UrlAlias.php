<?php

namespace amass\parsedata\core\entities\shop;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%url_alias}}".
 *
 * @property int $url_alias_id
 * @property string $query
 * @property string $keyword
 */
class UrlAlias extends ActiveRecord
{
  /**
   * @param $query
   * @param $keyword
   * @return static
   */
  public static function create($query, $keyword)
  {
    $alias = new static();
    $alias->query = $query;
    $alias->keyword = $keyword;
    return $alias;
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%url_alias}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['query', 'keyword'], 'required'],
      [['query', 'keyword'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'url_alias_id' => 'Url Alias ID',
      'query' => 'Query',
      'keyword' => 'Keyword',
    ];
  }
}
