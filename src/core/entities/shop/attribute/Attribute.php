<?php

namespace amass\parsedata\core\entities\shop\attribute;

use amass\parsedata\core\forms\manage\shop\attribute\AttributeDescriptionForm;
use amass\parsedata\core\forms\manage\shop\attribute\AttributeForm;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%attribute}}".
 *
 * @property int $attribute_id
 * @property int $attribute_group_id
 * @property int $sort_order
 *
 * @property AttributeDescription[] $attributeDescriptions
 */
class Attribute extends ActiveRecord
{
  /**
   * @param AttributeForm $attributeForm
   * @return static
   */
  public static function create(AttributeForm $attributeForm)
  {
    $object = new static();
    $object->attribute_group_id = $attributeForm->attribute_group_id;
    $object->sort_order = $attributeForm->sort_order;
    return $object;
  }
  /**
   * @param $languageId
   * @param AttributeDescriptionForm $attributeDescriptionForm
   */
  public function addDescription($languageId, AttributeDescriptionForm $attributeDescriptionForm)
  {
    $description = $this->attributeDescriptions;
    $description[] = AttributeDescription::create(
      $languageId,
      $attributeDescriptionForm->name
    );
    $this->attributeDescriptions = $description;
  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_group_id', 'sort_order'], 'required'],
            [['attribute_group_id', 'sort_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => 'Attribute ID',
            'attribute_group_id' => 'Attribute Group ID',
            'sort_order' => 'Sort Order',
        ];
    }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAttributeDescriptions()
  {
    return $this->hasMany(AttributeDescription::class, ['attribute_id' => 'attribute_id'])->orderBy('language_id');
  }

  /**
   * @return array
   */
  public function behaviors()
  {
    return [
      [
        'class' => SaveRelationsBehavior::className(),
        'relations' => ['attributeDescriptions'],
      ]
    ];
  }
}
