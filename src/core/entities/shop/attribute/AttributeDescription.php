<?php

namespace amass\parsedata\core\entities\shop\attribute;

use amass\parsedata\core\entities\shop\Language;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%attribute_description}}".
 *
 * @property int $attribute_id
 * @property int $language_id
 * @property string $name
 */
class AttributeDescription extends ActiveRecord
{
  /**
   * @param $languageId
   * @param $name
   * @return static
   */
  public static function create($languageId, $name)
  {
    $productDescription = new static();
    $productDescription->language_id = $languageId;
    $productDescription->name = $name;
    return $productDescription;
  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attribute_description}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'name'], 'required'],
            [['attribute_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['attribute_id', 'language_id'], 'unique', 'targetAttribute' => ['attribute_id', 'language_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => 'Attribute ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
        ];
    }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getLanguage()
  {
    return $this->hasOne(Language::class, ['language_id' => 'language_id']);
  }
}
