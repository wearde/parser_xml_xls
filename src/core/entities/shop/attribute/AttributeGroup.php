<?php

namespace amass\parsedata\core\entities\shop\attribute;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%attribute_group}}".
 *
 * @property int $attribute_group_id
 * @property int $sort_order
 */
class AttributeGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attribute_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_order'], 'required'],
            [['sort_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_group_id' => 'Attribute Group ID',
            'sort_order' => 'Sort Order',
        ];
    }
}
