<?php

namespace amass\parsedata\core\entities\shop\attribute;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%attribute_group_description}}".
 *
 * @property int $attribute_group_id
 * @property int $language_id
 * @property string $name
 */
class AttributeGroupDescription extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%attribute_group_description}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_group_id', 'language_id', 'name'], 'required'],
            [['attribute_group_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['attribute_group_id', 'language_id'], 'unique', 'targetAttribute' => ['attribute_group_id', 'language_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_group_id' => 'Attribute Group ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
        ];
    }
}
