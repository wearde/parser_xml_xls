<?php

namespace amass\parsedata\core\entities\shop\category;

use amass\parsedata\core\entities\Meta;
use amass\parsedata\core\forms\manage\shop\category\CategoryDescriptionForm;
use amass\parsedata\core\helpers\TimeHelper;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;

/**
 * Class Category
 * @package amass\parsedata\core\entities\shop\category
 */

/**
 * This is the model class for table "{{%category}}".
 *
 * @property int $category_id
 * @property string $image
 * @property int $parent_id
 * @property int $top
 * @property int $column
 * @property int $sort_order
 * @property int $status
 * @property string $date_added
 * @property string $date_modified
 *
 * @property int $owner_id
 * @property int $owner_parent_id
 *
 * @property CategoryDescription[] $categoryDescriptions
 * @property CategoryPath[] $categoryPath
 * @property CategoryToStore[] $categoryStore
 * @property Category $parent
 */

class Category extends ActiveRecord
{
  /**
   * @param $owner_id
   * @param $owner_parent_id
   * @param $parent_id
   * @return static
   */
  public static function create($owner_id, $owner_parent_id, $parent_id)
  {
    $category = new static();
    $category->owner_id = $owner_id;
    $category->owner_parent_id = $owner_parent_id;
    $category->parent_id = $parent_id;
    $category->status = 0;
    $category->top = 0;
    $category->column = 0;
    $category->date_added = TimeHelper::now();
    $category->date_modified = TimeHelper::now();

    return $category;
  }

  public function setTop()
  {
    $this->top = $this->parent ? 1 : 0;
  }

  /**
   * @param $languageId
   * @param CategoryDescriptionForm $categoryDescriptionForm
   */
  public function addDescription($languageId, CategoryDescriptionForm $categoryDescriptionForm)
  {
    $description = $this->categoryDescriptions;
    $description[] = CategoryDescription::create(
      $languageId,
      $categoryDescriptionForm->name,
      $categoryDescriptionForm->description,
      new Meta(
        $categoryDescriptionForm->meta->title,
        $categoryDescriptionForm->meta->description,
        $categoryDescriptionForm->meta->keywords
      )
    );
    $this->categoryDescriptions = $description;
  }

  /**
   * @param $category_id
   * @param $level
   */
  public function addPath($category_id, $level)
  {
    $path = $this->categoryPath;
    $path[] = CategoryPath::create(
      $category_id,
      $level
    );
    $this->categoryPath = $path;
  }

  /**
   * @param $parent
   * @param string $attribute
   * @return int
   */
  public function getPathLevel($parent, $attribute)
  {
    if ($parent) {
      /** @var CategoryPath $path */
      foreach ($parent->categoryPath as $path) {
        if ($path->isPathIdEqualTo($parent->{$attribute})) {
          return $path->level + 1;
        }
      }
    }

    return 0;
  }

  /**
   * @param $storeId
   */
  public function addStore($storeId)
  {
    $store = $this->categoryStore;
    $store[] = CategoryToStore::create(
      $storeId
    );
    $this->categoryStore = $store;
  }

  /**
   *
   */
  public function setEnabled()
  {
    $this->status = 1;
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%category}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['parent_id', 'owner_parent_id', 'top', 'column', 'sort_order', 'status', 'owner_id'], 'integer'],
      [['top', 'status', 'date_added', 'date_modified'], 'required'],
      [['date_added', 'date_modified'], 'safe'],
      [['image'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'category_id' => 'Category ID',
      'image' => 'Image',
      'parent_id' => 'Parent ID',
      'top' => 'Top',
      'column' => 'Column',
      'sort_order' => 'Sort Order',
      'status' => 'Status',
      'date_added' => 'Date Added',
      'date_modified' => 'Date Modified',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategoryDescriptions()
  {
    return $this->hasMany(CategoryDescription::class, ['category_id' => 'category_id'])->orderBy('language_id');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategoryPath()
  {
    return $this->hasMany(CategoryPath::class, ['category_id' => 'category_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategoryStore()
  {
    return $this->hasMany(CategoryToStore::class, ['category_id' => 'category_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getParent()
  {
    return $this->hasOne(Category::class, ['category_id' => 'parent_id']);
  }

  /**
   * @return array
   */
  public function behaviors()
  {
    return [
      [
        'class' => SaveRelationsBehavior::className(),
        'relations' => ['categoryDescriptions', 'categoryPath', 'categoryStore'],
      ]
    ];
  }

  /**
   * @param bool $insert
   * @param array $changedAttributes
   */
  public function afterSave($insert, $changedAttributes)
  {
    if (!$this->owner_id) {
      $this->owner_id = $this->category_id;
      $this->owner_parent_id = 0;
      if ($this->parent && !$this->owner_parent_id) {
        $this->owner_parent_id = $this->parent->category_id;
      }
    }
    $this->addPath($this->category_id, $this->getPathLevel($this->parent, 'category_id'));
    parent::afterSave($insert, $changedAttributes);

  }
}
