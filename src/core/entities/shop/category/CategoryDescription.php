<?php

namespace amass\parsedata\core\entities\shop\category;

use amass\parsedata\core\entities\behaviors\MetaBehavior;
use amass\parsedata\core\entities\behaviors\UrlAliasBehavior;
use amass\parsedata\core\entities\Meta;
use amass\parsedata\core\entities\shop\Language;
use yii\db\ActiveRecord;

/**
 * Class CategoryDescription
 * @package amass\parsedata\core\entities\shop\category
 */

/**
 * This is the model class for table "{{%category_description}}".
 *
 * @property int $category_id
 * @property int $language_id
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property int $owner_id
 *
 * @property Meta $meta
 */
class CategoryDescription extends ActiveRecord
{
  /**
   * @var
   */
  public $meta;

  /**
   * @param $languageId
   * @param $name
   * @param $description
   * @param Meta $meta
   * @return static
   */
  public static function create($languageId, $name, $description, Meta $meta)
  {
    $productDescription = new static();
    $productDescription->language_id = $languageId;
    $productDescription->name = $name;
    $productDescription->description = $description;
    $productDescription->meta = $meta;
    return $productDescription;
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%category_description}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['language_id', 'name'], 'required'],
      [['category_id', 'language_id'], 'integer'],
      [['description'], 'string'],
      [['name', 'meta_title', 'meta_description', 'meta_keyword'], 'string', 'max' => 255],
      [['meta_title', 'meta_description', 'meta_keyword'], 'default', 'value' => ''],
      [['meta_title', 'meta_description', 'meta_keyword'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'category_id' => 'Category ID',
      'language_id' => 'Language ID',
      'name' => 'Name',
      'description' => 'Description',
      'meta_title' => 'Meta Title',
      'meta_description' => 'Meta Description',
      'meta_keyword' => 'Meta Keyword',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getLanguage()
  {
    return $this->hasOne(Language::class, ['language_id' => 'language_id']);
  }

  /**
   * @return array
   */
  public function behaviors()
  {
    return [
      MetaBehavior::className(),
      [
        'class' => UrlAliasBehavior::className(),
        'attributeId' => 'category_id',
        'attributeField' => 'name'
      ]
    ];
  }
}
