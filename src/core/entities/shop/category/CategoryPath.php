<?php

namespace amass\parsedata\core\entities\shop\category;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%category_path}}".
 *
 * @property int $category_id
 * @property int $path_id
 * @property int $level
 */
class CategoryPath extends ActiveRecord
{
  /**
   * @param $parentId
   * @param $level
   * @return static
   */
  public static function create($parentId, $level)
  {
    $categoryPath = new static();
    $categoryPath->path_id = $parentId;
    $categoryPath->level = $level;
    return $categoryPath;
  }

  /**
   * @param $id
   * @return bool
   */
  public function isPathIdEqualTo($id)
  {
    return $this->path_id == $id;
  }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_path}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path_id', 'level'], 'required'],
            [['category_id', 'path_id', 'level'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'path_id' => 'Path ID',
            'level' => 'Level',
        ];
    }
}
