<?php

namespace amass\parsedata\core\entities\shop\category;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%category_to_store}}".
 *
 * @property int $category_id
 * @property int $store_id
 */
class CategoryToStore extends ActiveRecord
{
  /**
   * @param $storeId
   * @return static
   */
  public static function create($storeId)
  {
    $categoryStore = new static();
    $categoryStore->store_id = $storeId;
    return $categoryStore;
  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_to_store}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id'], 'required'],
            [['category_id', 'store_id'], 'integer'],
            [['category_id', 'store_id'], 'unique', 'targetAttribute' => ['category_id', 'store_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'store_id' => 'Store ID',
        ];
    }
}
