<?php

namespace amass\parsedata\core\entities\shop\filter;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%filter}}".
 *
 * @property int $filter_id
 * @property int $filter_group_id
 * @property int $sort_order
 */
class Filter extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%filter}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filter_group_id', 'sort_order'], 'required'],
            [['filter_group_id', 'sort_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'filter_id' => 'Filter ID',
            'filter_group_id' => 'Filter Group ID',
            'sort_order' => 'Sort Order',
        ];
    }
}
