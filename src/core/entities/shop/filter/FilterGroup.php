<?php

namespace amass\parsedata\core\entities\shop\filter;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%filter_group}}".
 *
 * @property int $filter_group_id
 * @property int $sort_order
 */
class FilterGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%filter_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_order'], 'required'],
            [['sort_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'filter_group_id' => 'Filter Group ID',
            'sort_order' => 'Sort Order',
        ];
    }
}
