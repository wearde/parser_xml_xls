<?php

namespace amass\parsedata\core\entities\shop\manufacturer;

use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%manufacturer}}".
 *
 * @property int $manufacturer_id
 * @property string $name
 * @property string $image
 * @property int $sort_order
 *
 * @property ManufacturerToStore[] $store
 */
class Manufacturer extends ActiveRecord
{
  public static function create($name, $image = '', $order = 0)
  {
    $manufacturer = new static();
    $manufacturer->name = trim($name);
    $manufacturer->image = $image;
    $manufacturer->sort_order = $order;
    return $manufacturer;
  }

  /**
   * @param $storeId
   */
  public function addStore($storeId)
  {
    $store = $this->store;
    $store[] = ManufacturerToStore::create(
      $storeId
    );
    $this->store = $store;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getStore()
  {
    return $this->hasMany(ManufacturerToStore::class, ['manufacturer_id' => 'manufacturer_id']);
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%manufacturer}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['sort_order'], 'integer'],
      [['name'], 'string', 'max' => 64],
      [['image'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'manufacturer_id' => 'Manufacturer ID',
      'name' => 'Name',
      'image' => 'Image',
      'sort_order' => 'Sort Order',
    ];
  }

  /**
   * @return array
   */
  public function behaviors()
  {
    return [
      [
        'class' => SaveRelationsBehavior::className(),
        'relations' => ['store'],
      ],
    ];
  }
}
