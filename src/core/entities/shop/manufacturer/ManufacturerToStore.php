<?php

namespace amass\parsedata\core\entities\shop\manufacturer;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%manufacturer_to_store}}".
 *
 * @property int $manufacturer_id
 * @property int $store_id
 */
class ManufacturerToStore extends ActiveRecord
{
  /**
   * @param $storeId
   * @return static
   */
  public static function create($storeId)
  {
    $store = new static();
    $store->store_id = $storeId;
    return $store;
  }

  /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%manufacturer_to_store}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id'], 'required'],
            [['manufacturer_id', 'store_id'], 'integer'],
            [['manufacturer_id', 'store_id'], 'unique', 'targetAttribute' => ['manufacturer_id', 'store_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manufacturer_id' => 'Manufacturer ID',
            'store_id' => 'Store ID',
        ];
    }
}
