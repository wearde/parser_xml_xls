<?php

namespace amass\parsedata\core\entities\shop\option;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%option}}".
 *
 * @property int $option_id
 * @property string $type
 * @property int $sort_order
 */
class Option extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'sort_order'], 'required'],
            [['sort_order'], 'integer'],
            [['type'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'type' => 'Type',
            'sort_order' => 'Sort Order',
        ];
    }
}
