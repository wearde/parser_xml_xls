<?php

namespace amass\parsedata\core\entities\shop\option;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%option_description}}".
 *
 * @property int $option_id
 * @property int $language_id
 * @property string $name
 */
class OptionDescription extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_description}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'language_id', 'name'], 'required'],
            [['option_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['option_id', 'language_id'], 'unique', 'targetAttribute' => ['option_id', 'language_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'Option ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
        ];
    }
}
