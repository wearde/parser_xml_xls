<?php

namespace amass\parsedata\core\entities\shop\option;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%option_value}}".
 *
 * @property int $option_value_id
 * @property int $option_id
 * @property string $image
 * @property int $sort_order
 */
class OptionValue extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'image', 'sort_order'], 'required'],
            [['option_id', 'sort_order'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_value_id' => 'Option Value ID',
            'option_id' => 'Option ID',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }
}
