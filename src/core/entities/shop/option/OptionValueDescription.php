<?php

namespace amass\parsedata\core\entities\shop\option;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%option_value_description}}".
 *
 * @property int $option_value_id
 * @property int $language_id
 * @property int $option_id
 * @property string $name
 */
class OptionValueDescription extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_value_description}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_value_id', 'language_id', 'option_id', 'name'], 'required'],
            [['option_value_id', 'language_id', 'option_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['option_value_id', 'language_id'], 'unique', 'targetAttribute' => ['option_value_id', 'language_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_value_id' => 'Option Value ID',
            'language_id' => 'Language ID',
            'option_id' => 'Option ID',
            'name' => 'Name',
        ];
    }
}
