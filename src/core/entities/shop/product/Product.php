<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities\shop\product;

use amass\parsedata\core\entities\Meta;
use amass\parsedata\core\entities\shop\category\Category;
use amass\parsedata\core\entities\shop\manufacturer\Manufacturer;
use amass\parsedata\core\entities\shop\OwnerImage;
use amass\parsedata\core\entities\traits\EventTrait;
use amass\parsedata\core\forms\manage\shop\product\ProductDescriptionForm;
use amass\parsedata\core\helpers\TimeHelper;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $product_id
 *
 * @property string $model
 * @property string $sku
 *
 * @property string $upc
 * @property string $ean
 * @property string $jan
 * @property string $isbn
 * @property string $mpn
 * @property string $location
 * @property int $quantity
 * @property int $stock_status_id
 * @property string $image
 * @property int $manufacturer_id
 * @property int $shipping
 * @property string $price
 * @property int $points
 * @property int $tax_class_id
 * @property string $date_available
 *
 * @property string $weight
 * @property int $weight_class_id
 * @property string $length
 * @property string $width
 * @property string $height
 * @property int $length_class_id
 *
 * @property int $subtract
 * @property int $minimum
 * @property int $sort_order
 * @property int $status
 * @property int $viewed
 * @property string $date_added
 * @property string $date_modified
 *
 * @property string $product_url
 *
 * @property ProductDescription[] $productDescriptions
 * @property Manufacturer $manufacturer
 * @property ProductToCategory[] $categoryAssignments
 * @property Category[] $categories
 * @property ProductImage[] $photos
 * @property ProductOwnerImage[] $ownerImages
 * @property ProductAttribute[] $productAttributes
 * @property ProductToStore[] $productStore
 */
class Product extends ActiveRecord
{
  use EventTrait;

  const STATUS_DRAFT = 0;
  const STATUS_ACTIVE = 1;

  /**
   * @param $model
   * @param $sku
   * @param $weight
   * @param int $quantity
   * @return static
   */
  public static function create($model, $sku, $weight, $quantity = 1)
  {
    $product = new static();
    $product->model = $model;
    $product->sku = $sku;
    $product->quantity = $quantity;
    $product->weight = $weight;
    $product->status = self::STATUS_DRAFT;
    $product->stock_status_id = 7;
    $product->date_available = TimeHelper::nowDate();
    $product->date_added = TimeHelper::now();
    $product->date_modified = TimeHelper::now();
    return $product;
  }

  /**
   * @param $status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }
  /**
   * @param $url
   */
  public function setOwnerUrl($url)
  {
    $this->product_url = $url;
  }

  /**
   * @param $new
   */
  public function setPrice($new)
  {
    $this->price = $new;
  }

  /**
   * @param $photo
   */
  public function setMainPhoto($photo)
  {
    $this->image = $photo;
  }

  /**
   * @param $languageId
   * @param ProductDescriptionForm $productDescriptionForm
   */
  public function addDescription($languageId, ProductDescriptionForm $productDescriptionForm)
  {
    $description = $this->productDescriptions;
    $description[] = ProductDescription::create(
      $languageId,
      $productDescriptionForm->name,
      $productDescriptionForm->description,
      new Meta(
        $productDescriptionForm->meta->title,
        $productDescriptionForm->meta->description,
        $productDescriptionForm->meta->keywords
      )
    );
    $this->productDescriptions = $description;
  }

  public function addProductAttribute($languageId, $attributeId, $value)
  {
    $productAttribute = $this->productAttributes;
    $productAttribute[] = ProductAttribute::create($languageId, $attributeId, $value);
    $this->productAttributes = $productAttribute;
  }

  /**
   * @param OwnerImage $image
   */
  public function assignImage(OwnerImage $image)
  {
    $ownerImages = $this->ownerImages;

    $ownerImages[] = ProductOwnerImage::create($image->name, $image->url);
    $this->ownerImages = $ownerImages;
  }

  /**
   * @param OwnerImage $image
   */
  public function assignPhotos(OwnerImage $image)
  {
    $ownerImages = $this->photos;

    $ownerImages[] = ProductImage::create($image->url);
    $this->photos = $ownerImages;
  }


  public function assignCategory($categoryId)
  {
    $categories = $this->categoryAssignments;
    $categories[] = ProductToCategory::create($categoryId);
    $this->categoryAssignments = $categories;
  }

  /**
   * @param $id
   */
  public function assignManufacture($id)
  {
    $this->manufacturer_id = $id;
  }

  /**
   * @param $storeId
   */
  public function addStore($storeId)
  {
    $store = $this->productStore;
    $store[] = ProductToStore::create(
      $storeId
    );
    $this->productStore = $store;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getManufacturer()
  {
    return $this->hasOne(Manufacturer::class, ['id' => 'manufacturer_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategoryAssignments()
  {
    return $this->hasMany(ProductToCategory::class, ['product_id' => 'product_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPhotos()
  {
    return $this->hasMany(ProductImage::class, ['product_id' => 'product_id'])->orderBy('sort_order');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getOwnerImages()
  {
    return $this->hasMany(ProductOwnerImage::class, ['product_id' => 'product_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductAttributes()
  {
    return $this->hasMany(ProductAttribute::class, ['product_id' => 'product_id'])->orderBy('language_id');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductDescriptions()
  {
    return $this->hasMany(ProductDescription::class, ['product_id' => 'product_id'])->orderBy('language_id');
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductStore()
  {
    return $this->hasMany(ProductToStore::class, ['product_id' => 'product_id']);
  }

  /**
   * @return array
   */
  public function behaviors()
  {
    return [
      [
        'class' => SaveRelationsBehavior::className(),
        'relations' => ['categoryAssignments', 'productDescriptions', 'ownerImages', 'productAttributes', 'photos', 'productStore'],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%product}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['model', 'sku', 'stock_status_id', 'manufacturer_id', 'date_added', 'date_modified'], 'required'],
      [['quantity', 'stock_status_id', 'manufacturer_id', 'shipping', 'points', 'tax_class_id', 'weight_class_id', 'length_class_id', 'subtract', 'minimum', 'sort_order', 'status', 'viewed'], 'integer'],
      [['price', 'weight', 'length', 'width', 'height'], 'number'],
      [['date_available', 'date_added', 'date_modified'], 'safe'],
      [['model', 'sku', 'mpn'], 'string', 'max' => 64],
      [['sku'], 'unique', 'targetClass' => Product::class],
      [['upc'], 'string', 'max' => 12],
      [['ean'], 'string', 'max' => 14],
      [['jan'], 'string', 'max' => 13],
      [['isbn'], 'string', 'max' => 17],
      [['location'], 'string', 'max' => 128],
      [['image', 'product_url'], 'string', 'max' => 255],
      [['upc', 'ean', 'mpn', 'jan', 'isbn', 'location'], 'default', 'value' => ''],
      [['shipping', 'tax_class_id'], 'default', 'value' => 1]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'product_id' => 'Product ID',
      'model' => 'Model',
      'sku' => 'Sku',
      'upc' => 'Upc',
      'ean' => 'Ean',
      'jan' => 'Jan',
      'isbn' => 'Isbn',
      'mpn' => 'Mpn',
      'location' => 'Location',
      'quantity' => 'Quantity',
      'stock_status_id' => 'Stock Status ID',
      'image' => 'Image',
      'manufacturer_id' => 'Manufacturer ID',
      'shipping' => 'Shipping',
      'price' => 'Price',
      'points' => 'Points',
      'tax_class_id' => 'Tax Class ID',
      'date_available' => 'Date Available',
      'weight' => 'Weight',
      'weight_class_id' => 'Weight Class ID',
      'length' => 'Length',
      'width' => 'Width',
      'height' => 'Height',
      'length_class_id' => 'Length Class ID',
      'subtract' => 'Subtract',
      'minimum' => 'Minimum',
      'sort_order' => 'Sort Order',
      'status' => 'Status',
      'viewed' => 'Viewed',
      'date_added' => 'Date Added',
      'date_modified' => 'Date Modified',
    ];
  }
}
