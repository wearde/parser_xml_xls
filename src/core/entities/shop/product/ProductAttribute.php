<?php

namespace amass\parsedata\core\entities\shop\product;

use amass\parsedata\core\entities\shop\attribute\Attribute;
use amass\parsedata\core\entities\shop\Language;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_attribute}}".
 *
 * @property int $product_id
 * @property int $attribute_id
 * @property int $language_id
 * @property string $text
 *
 * @property Language $language
 * @property Attribute $attributeOwner
 */
class ProductAttribute extends ActiveRecord
{
  /**
   * @param $languageId
   * @param $attributeId
   * @param $text
   * @return static
   */
  public static function create($languageId, $attributeId, $text)
  {
    $productAttributes = new static();
    $productAttributes->language_id = $languageId;
    $productAttributes->attribute_id = $attributeId;
    $productAttributes->text = $text;
    return $productAttributes;
  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_id', 'language_id', 'text'], 'required'],
            [['product_id', 'attribute_id', 'language_id'], 'integer'],
            [['text'], 'string'],
            [['product_id', 'attribute_id', 'language_id'], 'unique', 'targetAttribute' => ['product_id', 'attribute_id', 'language_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'attribute_id' => 'Attribute ID',
            'language_id' => 'Language ID',
            'text' => 'Text',
        ];
    }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAttributeOwner()
  {
    return $this->hasOne(Attribute::class, ['attribute_id' => 'attribute_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getLanguage()
  {
    return $this->hasOne(Language::class, ['language_id' => 'language_id']);
  }
}
