<?php

namespace amass\parsedata\core\entities\shop\product;

use amass\parsedata\core\entities\behaviors\MetaBehavior;
use amass\parsedata\core\entities\behaviors\UrlAliasBehavior;
use amass\parsedata\core\entities\Meta;
use amass\parsedata\core\entities\shop\Language;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_description}}".
 *
 * @property int $product_id
 * @property int $language_id
 * @property string $name
 * @property string $description
 * @property string $tag
 *
 * @property Meta $meta
 */
class ProductDescription extends ActiveRecord
{
  public $meta;

  /**
   * @param $languageId
   * @param $name
   * @param $description
   * @param Meta $meta
   * @return static
   */
  public static function create($languageId, $name, $description, Meta $meta)
  {
    $productDescription = new static();
    $productDescription->language_id = $languageId;
    $productDescription->name = $name;
    $productDescription->description = $description;
    $productDescription->meta = $meta;
    return $productDescription;
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%product_description}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['language_id', 'name'], 'required'],
      [['product_id', 'language_id'], 'integer'],
      [['description', 'tag'], 'string'],
      [['name', 'meta_title', 'meta_description', 'meta_keyword'], 'string', 'max' => 255],
      [['tag', 'meta_title', 'meta_description', 'meta_keyword'], 'default', 'value' => ''],
      [['tag', 'meta_title', 'meta_description', 'meta_keyword'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'product_id' => 'Product ID',
      'language_id' => 'Language ID',
      'name' => 'Name',
      'description' => 'Description',
      'tag' => 'Tag',
      'meta_title' => 'Meta Title',
      'meta_description' => 'Meta Description',
      'meta_keyword' => 'Meta Keyword',
    ];
  }

  public function getLanguage()
  {
    return $this->hasOne(Language::class, ['language_id' => 'language_id']);
  }

  public function behaviors()
  {
    return [
      MetaBehavior::className(),
      [
        'class' => UrlAliasBehavior::className(),
        'attributeId' => 'product_id',
        'attributeField' => 'name'
      ]
    ];
  }
}
