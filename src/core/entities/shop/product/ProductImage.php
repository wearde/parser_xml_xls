<?php

namespace amass\parsedata\core\entities\shop\product;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_image}}".
 *
 * @property int $product_image_id
 * @property int $product_id
 * @property string $image
 * @property int $sort_order
 */
class ProductImage extends ActiveRecord
{
  /**
   * @param $url
   * @return static
   */
  public static function create($url)
  {
    $image = new static();
    $image->image = $url;
    $image->sort_order = 0;
    return $image;

  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['product_id', 'sort_order'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_image_id' => 'Product Image ID',
            'product_id' => 'Product ID',
            'image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }
}
