<?php

namespace amass\parsedata\core\entities\shop\product;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_option}}".
 *
 * @property int $product_option_id
 * @property int $product_id
 * @property int $option_id
 * @property string $value
 * @property int $required
 */
class ProductOption extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'option_id', 'value', 'required'], 'required'],
            [['product_id', 'option_id', 'required'], 'integer'],
            [['value'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_option_id' => 'Product Option ID',
            'product_id' => 'Product ID',
            'option_id' => 'Option ID',
            'value' => 'Value',
            'required' => 'Required',
        ];
    }
}
