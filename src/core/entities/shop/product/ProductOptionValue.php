<?php

namespace amass\parsedata\core\entities\shop\product;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_option_value}}".
 *
 * @property int $product_option_value_id
 * @property int $product_option_id
 * @property int $product_id
 * @property int $option_id
 * @property int $option_value_id
 * @property int $quantity
 * @property int $subtract
 * @property string $price
 * @property string $price_prefix
 * @property int $points
 * @property string $points_prefix
 * @property string $weight
 * @property string $weight_prefix
 */
class ProductOptionValue extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_option_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_option_id', 'product_id', 'option_id', 'option_value_id', 'quantity', 'subtract', 'price', 'price_prefix', 'points', 'points_prefix', 'weight', 'weight_prefix'], 'required'],
            [['product_option_id', 'product_id', 'option_id', 'option_value_id', 'quantity', 'subtract', 'points'], 'integer'],
            [['price', 'weight'], 'number'],
            [['price_prefix', 'points_prefix', 'weight_prefix'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_option_value_id' => 'Product Option Value ID',
            'product_option_id' => 'Product Option ID',
            'product_id' => 'Product ID',
            'option_id' => 'Option ID',
            'option_value_id' => 'Option Value ID',
            'quantity' => 'Quantity',
            'subtract' => 'Subtract',
            'price' => 'Price',
            'price_prefix' => 'Price Prefix',
            'points' => 'Points',
            'points_prefix' => 'Points Prefix',
            'weight' => 'Weight',
            'weight_prefix' => 'Weight Prefix',
        ];
    }
}
