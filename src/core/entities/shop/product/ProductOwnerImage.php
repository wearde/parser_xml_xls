<?php

namespace amass\parsedata\core\entities\shop\product;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_owner_image}}".
 *
 * @property int $product_id
 * @property string $name
 * @property string $url
 */
class ProductOwnerImage extends ActiveRecord
{
  /**
   * @param $name
   * @param $url
   * @return static
   */
  public static function create($name, $url)
  {
    $image = new static();
    $image->name = $name;
    $image->url = $url;
    return $image;

  }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_owner_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['name', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'name' => 'Name',
            'url' => 'Url',
        ];
    }
}
