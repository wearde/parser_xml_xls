<?php

namespace amass\parsedata\core\entities\shop\product;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_to_category}}".
 *
 * @property int $product_id
 * @property int $category_id
 */
class ProductToCategory extends ActiveRecord
{
  /**
   * @param $category_id
   * @return static
   */
  public static function create($category_id)
  {
    $category = new static();
    $category->category_id = $category_id;
    return $category;
  }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id'], 'required'],
            [['product_id', 'category_id'], 'integer'],
            [['product_id', 'category_id'], 'unique', 'targetAttribute' => ['product_id', 'category_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'category_id' => 'Category ID',
        ];
    }
}
