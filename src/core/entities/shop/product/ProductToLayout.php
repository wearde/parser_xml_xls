<?php

namespace amass\parsedata\core\entities\shop\product;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_to_layout}}".
 *
 * @property int $product_id
 * @property int $store_id
 * @property int $layout_id
 */
class ProductToLayout extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_layout}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'store_id', 'layout_id'], 'required'],
            [['product_id', 'store_id', 'layout_id'], 'integer'],
            [['product_id', 'store_id'], 'unique', 'targetAttribute' => ['product_id', 'store_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'store_id' => 'Store ID',
            'layout_id' => 'Layout ID',
        ];
    }
}
