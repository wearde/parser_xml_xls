<?php

namespace amass\parsedata\core\entities\shop\product;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_to_store}}".
 *
 * @property int $product_id
 * @property int $store_id
 */
class ProductToStore extends ActiveRecord
{
  /**
   * @param $storeId
   * @return static
   */
  public static function create($storeId)
  {
    $productStore = new static();
    $productStore->store_id = $storeId;
    return $productStore;
  }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_store}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id'], 'required'],
            [['product_id', 'store_id'], 'integer'],
            [['product_id', 'store_id'], 'unique', 'targetAttribute' => ['product_id', 'store_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'store_id' => 'Store ID',
        ];
    }
}
