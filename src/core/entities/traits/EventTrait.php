<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\entities\traits;

/**
 * Trait EventTrait
 * @package amass\parsedata\core\entities\traits
 */
trait EventTrait
{
  /**
   * @var array
   */
  private $events = [];

  /**
   * @param $event
   */
  public function recordEvent($event)
  {
    $this->events[] = $event;
  }

  /**
   * @return array
   */
  public function releaseEvents()
  {
    $events = $this->events;
    $this->events = [];
    return $events;
  }
}