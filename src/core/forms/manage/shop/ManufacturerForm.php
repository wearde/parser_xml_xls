<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop;

use amass\parsedata\core\entities\shop\manufacturer\Manufacturer;
use yii\base\Model;

class ManufacturerForm extends Model
{
  public $name;
  public $sort_order;
  public $image;

  public function __construct(Manufacturer $manufacturer = null, $config = [])
  {
    if ($manufacturer) {
      $this->name = $manufacturer->name;
      $this->sort_order = $manufacturer->sort_order;
      $this->image = $manufacturer->image;
    }
    parent::__construct($config);
  }

  public function rules()
  {
    return [
      [['name'], 'required'],
      [['sort_order'], 'integer'],
      [['name'], 'string', 'max' => 64],
      [['image'], 'string', 'max' => 255],
    ];
  }
}