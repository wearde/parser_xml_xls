<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop;

use amass\parsedata\core\entities\Meta;
use yii\base\Model;

class MetaForm extends Model
{
  public $title;
  public $description;
  public $keywords;

  public function __construct(Meta $meta = null, $config = [])
  {
    if ($meta) {
      $this->title = $meta->meta_title;
      $this->description = $meta->meta_description;
      $this->keywords = $meta->meta_keyword;
    }
    parent::__construct($config);
  }

  public function rules()
  {
    return [
      [['title'], 'required'],
      [['title'], 'string', 'max' => 255],
      [['description', 'keywords'], 'string'],
    ];
  }
}