<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\attribute;

use amass\parsedata\core\entities\shop\attribute\AttributeDescription;
use yii\base\Model;

class AttributeDescriptionForm extends Model
{
  public $attribute_id;
  public $language_id;
  public $name;

  public function __construct(AttributeDescription $attributeDescription = null, $config = [])
  {
    if ($attributeDescription) {
      $this->name = $attributeDescription->name;
      $this->attribute_id = $attributeDescription->attribute_id;
      $this->language_id = $attributeDescription->language_id;
    }
    parent::__construct($config);
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['attribute_id', 'language_id'], 'integer'],
      [['name'], 'string', 'max' => 255],
    ];
  }
}