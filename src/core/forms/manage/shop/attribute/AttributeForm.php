<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\attribute;

use elisdn\compositeForm\CompositeForm;


/**
 * Class AttributeForm
 * @package amass\parsedata\core\forms\manage\shop\attribute
 */
/**
 * @property AttributeDescriptionForm $attributeDescription
 */
class AttributeForm extends CompositeForm
{
  /**
   * @var
   */
  public $attribute_group_id;
  /**
   * @var
   */
  public $sort_order;

  /**
   * AttributeForm constructor.
   * @param int $attribute_group_id
   * @param int $sort_order
   * @param array $config
   *
   */
  public function __construct($attribute_group_id = 1, $sort_order = 0, $config = [])
  {
    $this->attributeDescription = new AttributeDescriptionForm();
    $this->attribute_group_id = $attribute_group_id;
    $this->sort_order = $sort_order;
    parent::__construct($config);
  }

  /**
   * @return array
   */
  public function rules()
  {
    return [
      [['attribute_group_id', 'sort_order'], 'integer'],
    ];
  }

  /**
   * @return array
   */
  protected function internalForms()
  {
    return ['attributeDescription'];
  }
}