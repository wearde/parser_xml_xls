<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\category;

use elisdn\compositeForm\CompositeForm;

/**
 * Class CategoryForm
 * @package amass\parsedata\core\forms\manage\shop\category
 */

/**
 * @property CategoryDescriptionForm $categoryDescription
 */
class CategoryCreateForm extends CompositeForm
{

  /**
   * @var
   */
  public $owner_parent_id;
  /**
   * @var
   */
  public $owner_id;

  public $owner_parent_name;

  /**
   * CategoryForm constructor.
   * @param array $config
   */
  public function __construct($config = [])
  {
    $this->categoryDescription = new CategoryDescriptionForm();
    parent::__construct($config);
  }

  /**
   * @return array
   */
  public function rules()
  {
    return [
      ['owner_parent_name', 'string'],
      [['owner_parent_id', 'owner_id'], 'integer'],
    ];
  }

  /**
   * @return array
   */
  protected function internalForms()
  {
    return ['categoryDescription'];
  }
}