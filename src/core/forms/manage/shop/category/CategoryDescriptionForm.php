<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\category;

use amass\parsedata\core\entities\shop\category\CategoryDescription;
use amass\parsedata\core\forms\manage\shop\MetaForm;
use elisdn\compositeForm\CompositeForm;

/**
 * @property MetaForm $meta
 */

class CategoryDescriptionForm extends CompositeForm
{
  public $category_id;
  public $language_id;
  public $name;
  public $description;

  public function __construct(CategoryDescription $categoryDescription = null, $config = [])
  {
    if ($categoryDescription) {
      $this->name = $categoryDescription->name;
      $this->category_id = $categoryDescription->category_id;
      $this->description = $categoryDescription->description;
      $this->language_id = $categoryDescription->language_id;
      $this->meta = new MetaForm($categoryDescription->meta);
    } else {
      $this->meta = new MetaForm();
    }
    parent::__construct($config);
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['category_id', 'language_id'], 'integer'],
      [['description'], 'string'],
      [['name'], 'string', 'max' => 255],
    ];
  }


  protected function internalForms()
  {
    return ['meta'];
  }
}