<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\product;

use amass\parsedata\core\entities\shop\OwnerAttribute;
use amass\parsedata\core\entities\shop\product\Product;
use amass\parsedata\core\entities\shop\product\ProductAttribute;
use yii\base\Model;

class ProductAttributeForm extends Model
{
  /**@var OwnerAttribute[] */
  public $data;

  public function __construct(Product $product = null, $config = [])
  {
    if ($product) {
      $this->data= array_map(function (ProductAttribute $attribute) {
        return new OwnerAttribute(
          $attribute->attributeOwner->attributeDescriptions[0]->name,
          $attribute->text
        );
      },$product->productAttributes);
    }
    parent::__construct($config);
  }


  public function rules()
  {
    return [
      ['data', 'default', 'value' => []],
    ];
  }
}