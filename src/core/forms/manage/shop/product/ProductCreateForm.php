<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\product;

use amass\parsedata\core\entities\shop\product\Product;
use amass\parsedata\core\forms\manage\shop\ManufacturerForm;
use elisdn\compositeForm\CompositeForm;

/**
 * Class ProductCreateForm
 * @package amass\parsedata\core\forms\manage\shop\product
 */

/**
 * @property ProductDescriptionForm $productDescription
 * @property ManufacturerForm $manufacturer
 * @property ProductPriceForm $price
 * @property ProductOwnerImageForm $ownerImages
 * @property ProductAttributeForm $values
 * @property ProductImageForm $photos
 */
class ProductCreateForm extends CompositeForm
{
  public $model;
  public $sku;
  public $weight;
  public $product_url;
  public $category_id;
  public $category_name;
  public $quantity = 1;
  public $main_image;
  public $status = Product::STATUS_DRAFT;


  public function __construct($config = [])
  {
    $this->productDescription = new ProductDescriptionForm();
    $this->manufacturer = new ManufacturerForm();
    $this->price = new ProductPriceForm();
    $this->ownerImages = new ProductOwnerImageForm();
    $this->photos = new ProductImageForm();
    $this->values = new ProductAttributeForm();
    parent::__construct($config);
  }

  public function rules()
  {
    return [
      [['model', 'sku'], 'required'],
      [['product_url', 'category_name', 'main_image'], 'string', 'max' => 255],
      [['category_id', 'quantity'], 'integer'],
      [['weight'], 'number'],
    ];
  }


  protected function internalForms()
  {
    return ['productDescription', 'manufacturer', 'price', 'ownerImages', 'values', 'photos'];
  }
}