<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\product;

use amass\parsedata\core\entities\shop\product\ProductDescription;
use amass\parsedata\core\forms\manage\shop\MetaForm;
use elisdn\compositeForm\CompositeForm;

/**
 * @property MetaForm $meta
 */

class ProductDescriptionForm extends CompositeForm
{
  public $product_id;
  public $language_id;
  public $name;
  public $description;

  public function __construct(ProductDescription $product = null, $config = [])
  {
    if ($product) {
      $this->product_id = $product->product_id;
      $this->language_id = $product->language_id;
      $this->name = $product->name;
      $this->description = $product->description;
      $this->meta = new MetaForm($product->meta);
    } else {
      $this->meta = new MetaForm();
    }
    parent::__construct($config);
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name'], 'required'],
      [['product_id', 'language_id'], 'integer'],
      [['description'], 'string'],
      [['name'], 'string', 'max' => 255],
    ];
  }


  protected function internalForms()
  {
    return ['meta'];
  }
}