<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\product;

use amass\parsedata\core\entities\shop\product\Product;
use amass\parsedata\core\entities\shop\product\ProductOwnerImage;
use yii\base\Model;

class ProductOwnerImageForm extends Model
{
  public $images;

  public function __construct(Product $product = null, $config = [])
  {
    if ($product) {
      $this->images = array_map(function (ProductOwnerImage $image) {
        return [
          'name' => $image->name,
          'url' => $image->url
        ];
      },$product->ownerImages);
    }
    parent::__construct($config);
  }


  public function rules()
  {
    return [
      ['images', 'default', 'value' => []],
    ];
  }
}