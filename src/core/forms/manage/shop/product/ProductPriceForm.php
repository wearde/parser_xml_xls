<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\forms\manage\shop\product;

use amass\parsedata\core\entities\shop\product\Product;
use yii\base\Model;

class ProductPriceForm extends Model
{
  public $new;

  public function __construct(Product $product = null, $config = [])
  {
    if ($product) {
      $this->new = $product->price;
    }
    parent::__construct($config);
  }

  public function rules()
  {
    return [
      [['new'], 'required'],
      [['new'], 'integer', 'min' => 0],
    ];
  }
}