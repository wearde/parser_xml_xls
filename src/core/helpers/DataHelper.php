<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\helpers;

use yii\base\Model;

class DataHelper
{
  /**
   * @param Model[] $forms the model object
   * @return array
   */
  public static function field(array $forms)
  {
    $data = [];
    foreach ($forms as $form) {
      if (is_array($form)) {
        foreach ($form as $key => $item) {
          $data[$item->formName()][$key] = $item->attributes;
        }
      } else {
        $data[$form->formName()] = $form->attributes;
      }
    }
    return $data;
  }
}