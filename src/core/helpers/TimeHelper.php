<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\helpers;

class TimeHelper
{
  public static function now()
  {
    return (new \DateTime())->format('Y-m-d H:i:s');
  }

  public static function nowDate()
  {
    return (new \DateTime())->format('Y-m-d');
  }
}