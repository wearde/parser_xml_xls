<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\jobs;

/**
 * Class AsyncEventJob
 * @package amass\parsedata\core\jobs
 */
class AsyncEventJob extends Job
{
  /**
   * @var
   */
  public $event;

  /**
   * AsyncEventJob constructor.
   * @param $event
   */
  public function __construct($event)
  {
    $this->event = $event;
  }
}