<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\jobs;

use amass\parsedata\core\dispatchers\EventDispatcher;

class AsyncEventJobHandler
{
  private $dispatcher;

  /**
   * AsyncEventJobHandler constructor.
   * @param EventDispatcher $dispatcher
   */
  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }

  /**
   * @param AsyncEventJob $job
   */
  public function handle(AsyncEventJob $job)
  {
    $this->dispatcher->dispatch($job->event);
  }
}