<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\jobs;

/**
 * Class Job
 * @package amass\parsedata\core\jobs
 */
class Job implements \yii\queue\Job
{
  /**
   * @param \yii\queue\Queue $queue
   */
  public function execute($queue)
  {
    $listener = $this->resolveHandler();
    $listener($this, $queue);
  }

  /**
   * @return array
   */
  private function resolveHandler()
  {
    return [\Yii::createObject(static::class . 'Handler'), 'handle'];
  }
}