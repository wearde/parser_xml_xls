<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\listeners\shop\product;

use amass\parsedata\core\entities\events\LoadOwnerImage;
use amass\parsedata\core\entities\shop\product\Product;
use amass\parsedata\core\entities\shop\product\ProductImage;
use amass\parsedata\core\repositories\manager\shop\ProductRepository;
use yii\console\ErrorHandler;
use yii\helpers\BaseFileHelper;

/**
 * Class LoadOwnerImageListener
 * @package amass\parsedata\core\listeners\shop\product
 */
class LoadOwnerImageListener
{
  /**
   * @var ErrorHandler
   */
  private $errorHandler;
  /**
   * @var ProductRepository
   */
  private $productRepository;

  /**
   * LoadOwnerImageListener constructor.
   * @param ProductRepository $productRepository
   * @param ErrorHandler $errorHandler
   */
  public function __construct(ProductRepository $productRepository, ErrorHandler $errorHandler)
  {

    $this->errorHandler = $errorHandler;
    $this->productRepository = $productRepository;
  }

  /**
   * @param LoadOwnerImage $event
   */
  public function handle(LoadOwnerImage $event)
  {
    try {
      foreach ($event->images as $image) {
        $file = $this->loadImage($image);
        $imagePath = DIR_IMAGE . $event->product->product_id;
        BaseFileHelper::createDirectory($imagePath, 0777, true);

        $event->product->status = Product::STATUS_ACTIVE;

        if (!$event->product->image) {
          $event->product->image = $event->product->product_id . '/' . $image->name;
          $event->product->save(false, ['image']);
        } else {
          $productImage = new ProductImage();
          $productImage->product_id = $event->product->product_id;
          $productImage->image = $event->product->product_id . '/' . $image->name;
          $productImage->sort_order = 0;
          $productImage->save();
        }
        file_put_contents($imagePath . '/'. $image->name, $file);
      }

    } catch (\Exception $e) {
      $this->errorHandler->handleException($e);
    }
  }

  private function loadImage($image)
  {
    $headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
    $headers[] = 'Connection: Keep-Alive';
    $headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
    $user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36';
    $process = curl_init($image->url);
    curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($process, CURLOPT_HEADER, 0);
    curl_setopt($process, CURLOPT_USERAGENT, $user_agent); //check here
    curl_setopt($process, CURLOPT_TIMEOUT, 30);
    curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
    $return = curl_exec($process);
    curl_close($process);
    return $return;
  }
}