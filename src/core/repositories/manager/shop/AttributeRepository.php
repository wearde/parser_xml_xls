<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\repositories\manager\shop;

use amass\parsedata\core\entities\shop\attribute\Attribute;
use amass\parsedata\core\entities\shop\attribute\AttributeDescription;
use yii\db\Query;
use yii\web\NotFoundHttpException;

class AttributeRepository
{
  /**
   * @param $id
   * @return Attribute
   * @throws NotFoundHttpException
   */
  public function get($id)
  {
    if (!$manufacturer = Attribute::findOne($id)) {
      throw new NotFoundHttpException('Attribute is not found.');
    }
    return $manufacturer;
  }

  /**
   * @param $name
   * @return array|null|\yii\db\ActiveRecord|Attribute
   */
  public function findByName($name)
  {
    return Attribute::find()
      ->joinWith(['attributeDescriptions' => function ($q) use ($name) {
        /**@var Query $q */
        $q->where(['name' => trim($name)]);
      }])
      ->one();
  }

  /**
   * @return int
   */
  public function deleteAllDescription()
  {
    return AttributeDescription::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAll()
  {
    return Attribute::deleteAll();
  }
}

