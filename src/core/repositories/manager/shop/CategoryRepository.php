<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\repositories\manager\shop;

use amass\parsedata\core\entities\shop\category\Category;
use amass\parsedata\core\entities\shop\category\CategoryDescription;
use amass\parsedata\core\entities\shop\category\CategoryPath;
use amass\parsedata\core\entities\shop\category\CategoryToStore;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * Class CategoryRepository
 * @package amass\parsedata\core\repositories\manager\shop
 */
class CategoryRepository
{
  /**
   * @param $id
   * @return Category
   * @throws NotFoundHttpException
   */
  public function get($id)
  {
    if (!$category = Category::findOne($id)) {
      throw new NotFoundHttpException('Category is not found.');
    }
    return $category;
  }

  /**
   * @param $id
   * @param string $name
   * @return array|null|\yii\db\ActiveRecord|Category
   */
  public function findParentByOwner($id, $name = '')
  {
    if ($id) {
      return Category::find()
        ->where(['owner_id' => $id])
        ->one();
    }
    return Category::find()
      ->joinWith(['categoryDescriptions' => function ($q) use ($name) {
        /**@var Query $q */
        $q->where(['name' => trim($name)]);
      }])
      ->one();
  }

  /**
   * @param $id
   * @return bool
   */
  public function existsByOwner($id)
  {
    return Category::find()
      ->andWhere(['owner_id' => $id])
      ->exists();
  }

  /**
   * @param $id
   * @param string $name
   * @return array|null|\yii\db\ActiveRecord
   */
  public function findByOwner($id, $name = '')
  {
    if ($id) {
      return Category::find()
        ->andWhere(['owner_id' => $id])
        ->one();
    }
    return Category::find()
      ->joinWith(['categoryDescriptions' => function ($q) use ($name) {
        /**@var Query $q */
        $q->where(['name' => trim($name)]);
      }])
      ->one();

  }

  /**
   * @param Category $category
   */
  public function save(Category $category)
  {

    if (!$category->save()) {
      print_r($category->getErrors());
      throw new \RuntimeException('Saving error.');
    }
  }

  /**
   * @return int
   */
  public function deleteAllStore()
  {
    return CategoryToStore::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllPath()
  {
    return CategoryPath::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllDescription()
  {
    return CategoryDescription::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAll()
  {
    return Category::deleteAll();
  }
}