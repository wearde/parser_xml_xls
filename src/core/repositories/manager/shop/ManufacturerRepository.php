<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\repositories\manager\shop;

use amass\parsedata\core\entities\shop\manufacturer\Manufacturer;
use amass\parsedata\core\entities\shop\manufacturer\ManufacturerToStore;
use yii\web\NotFoundHttpException;

/**
 * Class ManufacturerRepository
 * @package amass\parsedata\core\repositories\manager\shop
 */
class ManufacturerRepository
{
  /**
   * @param $id
   * @return Manufacturer
   * @throws NotFoundHttpException
   */
  public function get($id)
  {
    if (!$manufacturer = Manufacturer::findOne($id)) {
      throw new NotFoundHttpException('Manufacturer is not found.');
    }
    return $manufacturer;
  }

  /**
   * @param $name
   * @return array|null|\yii\db\ActiveRecord|Manufacturer
   */
  public function findByName($name)
  {
    return Manufacturer::find()
      ->where(['name' => trim($name)])
      ->one();
  }

  /**
   * @return int
   */
  public function deleteAll()
  {
    return Manufacturer::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllStore()
  {
    return ManufacturerToStore::deleteAll();
  }
}