<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\repositories\manager\shop;

use amass\parsedata\core\dispatchers\EventDispatcher;
use amass\parsedata\core\entities\shop\product\Product;
use amass\parsedata\core\entities\shop\product\ProductAttribute;
use amass\parsedata\core\entities\shop\product\ProductDescription;
use amass\parsedata\core\entities\shop\product\ProductImage;
use amass\parsedata\core\entities\shop\product\ProductOwnerImage;
use amass\parsedata\core\entities\shop\product\ProductToCategory;
use amass\parsedata\core\entities\shop\product\ProductToStore;
use yii\web\NotFoundHttpException;

/**
 * Class ProductRepository
 * @package amass\parsedata\core\repositories\manager\shop
 */
class ProductRepository
{
  /**
   * @var EventDispatcher
   */
  private $dispatcher;

  /**
   * ProductRepository constructor.
   * @param EventDispatcher $dispatcher
   */
  public function __construct(EventDispatcher $dispatcher)
  {
    $this->dispatcher = $dispatcher;
  }

  /**
   * @param $id
   * @return Product
   * @throws NotFoundHttpException
   */
  public function get($id)
  {
    if (!$product = Product::findOne($id)) {
      throw new NotFoundHttpException('Product is not found.');
    }
    return $product;
  }

  /**
   * @param $sku
   * @return bool
   */
  public function existBySku($sku)
  {
    return Product::find()->andWhere(['sku' => trim($sku)])->exists();
  }

  /**
   * @param Product $product
   */
  public function save(Product $product)
  {
    if (!$product->save()) {
      throw new \RuntimeException('Saving error.');
    }
    $this->dispatcher->dispatchAll($product->releaseEvents());
  }

  /**
   * @param Product $product
   */
  public function remove(Product $product)
  {
    if (!$product->delete()) {
      throw new \RuntimeException('Removing error.');
    }
  }

  /**
   * @return int
   */
  public function deleteAllStore()
  {
    return ProductToStore::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllDescription()
  {
    return ProductDescription::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllOwnerImage()
  {
    return ProductOwnerImage::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllPhoto()
  {
    return ProductImage::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllAssignCategory()
  {
    return ProductToCategory::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAllAssignAttributes()
  {
    return ProductAttribute::deleteAll();
  }

  /**
   * @return int
   */
  public function deleteAll()
  {
    return Product::deleteAll();
  }
}