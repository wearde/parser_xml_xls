<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\repositories\read;

use amass\parsedata\core\entities\shop\Language;

class LanguageRepository
{
  /**
   * @param $id
   * @return Language|null
   */
  public function find($id)
  {
    return Language::findOne($id);
  }

  /**
   * @return array|\yii\db\ActiveRecord[]|Language[]
   */
  public function findAll()
  {
    return Language::find()->all();
  }
}