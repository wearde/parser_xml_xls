<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\services\manager\shop;

use amass\parsedata\core\entities\shop\category\Category;
use amass\parsedata\core\forms\manage\shop\category\CategoryCreateForm;
use amass\parsedata\core\repositories\manager\shop\CategoryRepository;
use amass\parsedata\core\repositories\read\LanguageRepository;
use amass\parsedata\core\services\TransactionService;

/**
 * Class CategoryManagerService
 * @package amass\parsedata\core\services\manager\shop
 */
class CategoryManagerService
{
  /**
   * @var CategoryRepository
   */
  private $categoryRepository;
  /**
   * @var LanguageRepository
   */
  private $languageRepository;
  /**
   * @var TransactionService
   */
  private $transactionService;

  /**
   * CategoryManagerService constructor.
   * @param CategoryRepository $categoryRepository
   * @param LanguageRepository $languageRepository
   * @param TransactionService $transactionService
   */
  public function __construct(
    CategoryRepository $categoryRepository,
    LanguageRepository $languageRepository,
    TransactionService $transactionService
  )
  {
    $this->categoryRepository = $categoryRepository;
    $this->languageRepository = $languageRepository;
    $this->transactionService = $transactionService;
  }

  /**
   * @param CategoryCreateForm $form
   * @return Category
   */
  public function create(CategoryCreateForm $form)
  {
    if (!$category = $this->categoryRepository->findByOwner($form->owner_id, $form->categoryDescription->name)) {
      /** @var Category $parent */
      $parent = $this->categoryRepository->findParentByOwner($form->owner_parent_id, $form->owner_parent_name);

      $category = Category::create(
        $form->owner_id,
        $form->owner_parent_id,
        $parent ? $parent->category_id : 0
      );
      foreach ($this->languageRepository->findAll() as $language) {
        $category->addDescription($language->language_id, $form->categoryDescription);
      }

      while ($parent) {
        $category->addPath($parent->category_id, $category->getPathLevel($parent, 'parent_id'));
        $parent = $parent->parent;
      }

      $category->addStore(0);
      $category->setEnabled();
      $category->setTop();

      $this->transactionService->wrap(function () use ($category) {
        $this->categoryRepository->save($category);
      });
    }
    return $category;
  }

  public function clearData()
  {
    $this->categoryRepository->deleteAllStore();
    $this->categoryRepository->deleteAllPath();
    $this->categoryRepository->deleteAllDescription();
    $this->categoryRepository->deleteAll();
  }
}