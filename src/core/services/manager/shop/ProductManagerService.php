<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\services\manager\shop;

use amass\parsedata\core\entities\events\LoadOwnerImage;
use amass\parsedata\core\entities\shop\attribute\Attribute;
use amass\parsedata\core\entities\shop\category\Category;
use amass\parsedata\core\entities\shop\manufacturer\Manufacturer;
use amass\parsedata\core\entities\shop\OwnerImage;
use amass\parsedata\core\entities\shop\product\Product;
use amass\parsedata\core\forms\manage\shop\attribute\AttributeForm;
use amass\parsedata\core\forms\manage\shop\product\ProductCreateForm;
use amass\parsedata\core\repositories\manager\shop\AttributeRepository;
use amass\parsedata\core\repositories\manager\shop\CategoryRepository;
use amass\parsedata\core\repositories\manager\shop\ManufacturerRepository;
use amass\parsedata\core\repositories\manager\shop\ProductRepository;
use amass\parsedata\core\repositories\read\LanguageRepository;
use amass\parsedata\core\services\TransactionService;

class ProductManagerService
{
  /**
   * @var ProductRepository
   */
  private $productRepository;
  /**
   * @var LanguageRepository
   */
  private $languageRepository;
  /**
   * @var TransactionService
   */
  private $transactionService;
  /**
   * @var ManufacturerRepository
   */
  private $manufacturerRepository;

  /**
   * @var ManufacturerRepository
   */
  private $attributeRepository;
  private $categoryRepository;

  /**
   * ProductManagerService constructor.
   * @param ProductRepository $productRepository
   * @param LanguageRepository $languageRepository
   * @param TransactionService $transactionService
   * @param ManufacturerRepository $manufacturerRepository
   * @param AttributeRepository $attributeRepository
   * @param CategoryRepository $categoryRepository
   */
  public function __construct(
    ProductRepository $productRepository,
    LanguageRepository $languageRepository,
    TransactionService $transactionService,
    ManufacturerRepository $manufacturerRepository,
    AttributeRepository $attributeRepository,
    CategoryRepository $categoryRepository
  )
  {
    $this->productRepository = $productRepository;
    $this->languageRepository = $languageRepository;
    $this->transactionService = $transactionService;
    $this->manufacturerRepository = $manufacturerRepository;
    $this->attributeRepository = $attributeRepository;
    $this->categoryRepository = $categoryRepository;
  }

  /**
   * @param ProductCreateForm $form
   * @return Product
   */
  public function create(ProductCreateForm $form)
  {
    if ($product = !$this->productRepository->existBySku($form->sku)) {

      $product = Product::create(
        $form->model,
        $form->sku,
        $form->weight,
        $form->quantity
      );

      $product->setPrice($form->price->new);
      $product->setStatus($form->status);
      $product->setMainPhoto($form->main_image);
      $product->setOwnerUrl($form->product_url);
      /**@var Category $category */
      $category = $this->categoryRepository->findByOwner($form->category_id, $form->category_name);
      if ($category) {
        $form->category_id = $category->category_id;
        $product->assignCategory($form->category_id);
      }

      /** @var OwnerImage $image */
      if ($form->ownerImages) {
        foreach ($form->ownerImages->images as $image) {
          $product->assignImage($image);
        }
        $product->recordEvent(new LoadOwnerImage($product, $form->ownerImages->images));
      }


      /** @var OwnerImage $image */
      foreach ($form->photos->images as $image) {
        $product->assignPhotos($image);
      }

      foreach ($this->languageRepository->findAll() as $language) {
        $product->addDescription($language->language_id, $form->productDescription);
      }

      $this->transactionService->wrap(function () use ($product, $form) {
        if (!$manufacturer = $this->manufacturerRepository->findByName($form->manufacturer->name)) {
          $manufacturer = Manufacturer::create($form->manufacturer->name);
          $manufacturer->addStore(0);
          $manufacturer->save();
        }
        $product->assignManufacture($manufacturer->manufacturer_id);

        $product->addStore(0);

        if ($form->values->data) {
          foreach ($form->values->data as $data) {
            if (!$attribute = $this->attributeRepository->findByName($data->name)) {
              $attributeForm = new AttributeForm(4);
              $attribute = Attribute::create($attributeForm);
              foreach ($this->languageRepository->findAll() as $language) {
                $attributeForm->attributeDescription->language_id = $language->language_id;
                $attributeForm->attributeDescription->name = $data->name;
                $attribute->addDescription($language->language_id, $attributeForm->attributeDescription);
              }
              $attribute->save();
            }
            foreach ($this->languageRepository->findAll() as $language) {
              $product->addProductAttribute($language->language_id, $attribute->attribute_id, $data->value);
            }
          }
        }

        $this->productRepository->save($product);
      });
    }
    return $product;
  }

  public function clearData()
  {
    $this->productRepository->deleteAllStore();
    $this->productRepository->deleteAllDescription();
    $this->productRepository->deleteAllOwnerImage();
    $this->productRepository->deleteAllPhoto();
    $this->productRepository->deleteAllAssignCategory();
    $this->productRepository->deleteAllAssignAttributes();
    $this->manufacturerRepository->deleteAllStore();
    $this->manufacturerRepository->deleteAll();
    $this->attributeRepository->deleteAllDescription();
    $this->attributeRepository->deleteAll();
    $this->productRepository->deleteAll();
  }
}