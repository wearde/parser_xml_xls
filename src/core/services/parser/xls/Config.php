<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\services\parser\xls;

class Config
{
  const CATEGORY = 0;
  const SUB_CATEGORY = 1;
  const SKU = 2;
  const NAME = 3;
  const FULL_CATEGORIES = 4;
  const PRICE = 5;
  const QUANTITY = 6;
  const MANUFACTURER = 7;
  const IMAGES = 8;
  const ATTRIBUTES = 9;

}