<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\services\parser\xls;

class ReadFilter implements \PHPExcel_Reader_IReadFilter
{
  private $startRow = 0;
  private $endRow = 0;

  public function setRows($startRow, $chunkSize) {
    $this->startRow = $startRow;
    $this->endRow = $startRow + $chunkSize;
  }

  public function readCell($column, $row, $worksheetName = '')
  {
    if ($row === 1 || ($row >= $this->startRow && $row < $this->endRow)) {
      return true;
    }
    return false;
  }
}