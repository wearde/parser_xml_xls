<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\services\parser\xml;

class DecoratorManager
{
  public function wrap(callable $function)
  {
    $transaction = \Yii::$app->db->beginTransaction();
    try {
      $this->dispatcher->defer();
      $function();
      $transaction->commit();
      $this->dispatcher->release();
    } catch (\Exception $e) {
      $transaction->rollBack();
      $this->dispatcher->clean();
      throw $e;
    }
  }
}