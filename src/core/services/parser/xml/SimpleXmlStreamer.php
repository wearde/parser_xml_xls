<?php
/**
 * This file is part of the OpenCart product loader
 *
 * @copyright 2017 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace amass\parsedata\core\services\parser\xml;


class SimpleXmlStreamer extends XmlStreamerAbstract
{
  /**
   * @var callable $callback Function
   */
  private $callbackFunction;

  /**
   * SimpleXmlStreamer constructor.
   * @param $mixed
   * @param int $chunkSize
   * @param null $totalBytes
   */
  public function __construct($mixed, $chunkSize = 16384, $totalBytes = null)
  {
    parent::__construct($mixed, $chunkSize, $totalBytes);
  }

  public function setCallBack(callable $callbackFunction)
  {
    $this->callbackFunction = $callbackFunction;
    return $this;
  }

  public function processNode($xmlString, $elementName, $nodeIndex)
  {
    $callback = $this->callbackFunction;
    $xml = simplexml_load_string($xmlString);
    if (is_callable($callback)) {
      $callback($xml);
    }
    return true;
  }
}