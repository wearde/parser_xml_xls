<?php

use yii\db\Migration;

/**
 * Handles adding owners to table `category`.
 */
class m170721_180920_add_owners_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%category}}', 'owner_id', $this->integer());
        $this->addColumn('{{%category}}', 'owner_parent_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%category}}', 'owner_id');
        $this->dropColumn('{{%category}}', 'owner_parent_id');
    }
}
