<?php

use yii\db\Migration;

/**
 * Handles adding product_url to table `{{%product}}`.
 */
class m170722_151937_add_product_url_column_to_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
      $this->alterColumn('{{%product}}', 'date_available', $this->date()->null());
        $this->addColumn('{{%product}}', 'product_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%product}}', 'product_url');
    }
}
