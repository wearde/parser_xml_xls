<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_owner_image}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%product}}`
 */
class m170722_152257_create_product_owner_image_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%product_owner_image}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(11)->notNull(),
            'name' => $this->string(),
            'url' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
      $this->dropTable('{{%product_owner_image}}');
    }
}
